﻿using UnityEngine;
using System.Collections;

public class Heart : MonoBehaviour {

    // Use this for initialization
    public int amount;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            DestroyObject(this.gameObject);
            hit.gameObject.GetComponent<Player>().Heal(amount);
        }
    }
}
