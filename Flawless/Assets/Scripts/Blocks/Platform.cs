﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{
    private EdgeCollider2D real;
    void Start()
    {
        real = GetComponents<EdgeCollider2D>()[0];
    }

    void Update()
    {
        if (Time.timeScale == 0) return;
        if (Input.GetButtonDown("Vertical"))
            real.enabled = false;
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.CompareTag("Ground"))
        {
            real.enabled = true;
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.CompareTag("Ground"))
            real.enabled = false;
    }
}
