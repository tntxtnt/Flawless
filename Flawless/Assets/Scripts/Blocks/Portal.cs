﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour
{
    public float newPointX;
    public float newPointY;
    public float newCameraX;
    public float newCameraY;

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player") && Input.GetButtonDown("Vertical"))
            run();
    }

    void OnTriggerStay2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player") && Input.GetButtonDown("Vertical"))
            run();
    }

    private void run()
    {
        GameObject player = GameObject.Find("Player");
        GameObject camera = GameObject.Find("Main Camera");
        player.transform.position = new Vector3(newPointX, newPointY, player.transform.position.z);
        camera.transform.position = new Vector3(newCameraX, newCameraY, camera.transform.position.z);
    }
}
