﻿using UnityEngine;
using System.Collections;

public class SaveBlock : MonoBehaviour
{
    
    public string LevelName;
    public string SongName;

    bool on;

    void Start()
    {
        on = false;
    }
    
    void Update()
    {
        if (Input.GetAxisRaw("Vertical") == 1 && on)
        {
            GameManager.instance.Save(LevelName, SongName);
        }
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            on = true;
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            on = false;
    }
}
