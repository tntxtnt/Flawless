﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource sfxSource;

    public AudioClip[] backgroundMusics;
    public string[] backgroundMusicsName;

    public AudioClip buttonSelect1;
    public AudioClip buttonSelect2;

    public static SoundManager instance = null;

    void Awake() 
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    
    public void PlaySingle(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }
    public void PlayMusic(string songName)
    {
        for (int i = 0; i < SoundManager.instance.backgroundMusicsName.Length; ++i)
        {
            if (SoundManager.instance.backgroundMusicsName[i] == songName)
            {
                if (SoundManager.instance.musicSource.clip == SoundManager.instance.backgroundMusics[i])
                    break;
                SoundManager.instance.musicSource.Stop();
                SoundManager.instance.musicSource.clip = SoundManager.instance.backgroundMusics[i];
                SoundManager.instance.musicSource.Play();
            }
        }
    }
}
