﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public bool first;
    public float newXpos = 0;
    public float newYpos = 0;
    public float newCamXpos = 0;
    public float newCamYpos = 0;
    public int PlayerFlip = 1;
    public int GlobalAttackUp = 0;
    public int PlayerHp = 10;
    public int PlayerMaxHp = 10;
    public bool[] GottenPowerUP;

    public static GameManager instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            first = true;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public void Load(string name, string songName)
    {
        if(!name.Equals("MainMenu"))
            first = false;
        Application.LoadLevel(name);
        SoundManager.instance.PlayMusic(songName);
    }
    public void addAttack(int attackUp)
    {
        GlobalAttackUp+=attackUp;
        GameObject.Find("Player").GetComponent<Player>().attackup();
    }
    public void addHp(int HpUp)
    {
        PlayerMaxHp += HpUp;
        if (PlayerMaxHp > 100)
            PlayerMaxHp = 100;
        GameObject.Find("Player").GetComponent<Player>().MaxHpup();
        GameObject.Find("Player").GetComponent<Player>().Heal(HpUp);
    }
    public void reset()
    {
        newXpos = 0;
        newYpos = 0;
        PlayerFlip = 1;
        GlobalAttackUp = 0;
        PlayerHp = 10;
        PlayerMaxHp = 10;
        int size = GottenPowerUP.Length;
        for (int i = 0; i < size; i++)
            GottenPowerUP[i]= false;
        first = true;
    }
    public void Save(string LevelName,string SongName)
    {
        string[] text = new string[12];
        text[0] = LevelName;
        text[1] = ""+GameObject.Find("Player").transform.position.x;
        text[2] = ""+GameObject.Find("Player").transform.position.y;
        text[3] = ""+GameObject.Find("Player").GetComponent<Player>().getFlip(); ;
        text[4] = "" + GlobalAttackUp;
        text[5] = "" + GameObject.Find("Player").GetComponent<Player>().getHP();
        text[6] = "" + PlayerMaxHp;
        int size = GottenPowerUP.Length;
        text[7] = "" + size;
        text[8] = "";
        for (int i=0;i<size;i++)
        {
            if(GottenPowerUP[i])
                text[8] += "1";
            else
                text[8] += "0";
        }
        text[9] = SongName;
        text[10] = "" + GameObject.Find("Main Camera").transform.position.x;
        text[11] = "" + GameObject.Find("Main Camera").transform.position.y;
        System.IO.File.WriteAllLines(@"Save.txt", text);
    }
    public void LoadSave()
    {
        if (System.IO.File.Exists(@"Save.txt"))
        {
            string[] lines = System.IO.File.ReadAllLines(@"Save.txt");
            string name = lines[0];
            newXpos = float.Parse(lines[1]);
            newYpos = float.Parse(lines[2]);
            PlayerFlip = int.Parse(lines[3]);
            GlobalAttackUp = int.Parse(lines[4]);
            PlayerHp = int.Parse(lines[5]);
            PlayerMaxHp = int.Parse(lines[6]);
            int size = int.Parse(lines[7]);
            GottenPowerUP = new bool[size];
            for (int i = 0; i < size; i++)
            {
                if (lines[8][i] == '1')
                    GottenPowerUP[i] = true;
                else
                    GottenPowerUP[i] = false;
            }
            string song = lines[9];
            newCamXpos = float.Parse(lines[10]);
            newCamYpos = float.Parse(lines[11]);
            Load(name, song);
            first = false;
        }
    }
}
