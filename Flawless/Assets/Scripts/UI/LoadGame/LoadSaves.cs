﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;


public class LoadSaves : MonoBehaviour
{
    public RectTransform content;
    public GameObject gameInfoPanel;

    private string savePath = "./Saves/";
    private string saveType = ".txt";

    void Start()
    {
        if (!Directory.Exists(savePath))
            Directory.CreateDirectory(savePath);
        DirectoryInfo saveDir = new DirectoryInfo(savePath);
        FileInfo[] files = saveDir.GetFiles("*" + saveType);

        float buttonDistance = 110f;
        content.sizeDelta = new Vector2(0, 30f + buttonDistance * files.Length);

        for (int i = 0; i < files.Length; ++i)
        {
            try
            {
                StreamReader fin = new StreamReader(files[i].FullName);
                GameObject savedGame = Instantiate(gameInfoPanel);
                savedGame.transform.SetParent(content);
                savedGame.transform.localPosition = new Vector3(0f, -65f - buttonDistance * i, 0f);
                savedGame.transform.localScale = Vector3.one;
                savedGame.name = "Save " + i;

                savedGame.transform.FindChild("GameName").gameObject.GetComponent<Text>().text = fin.ReadLine();
                savedGame.transform.FindChild("Difficulty").gameObject.GetComponent<Text>().text = fin.ReadLine();
                bool hardcore = fin.ReadLine()[0] == '1';
                var hc = savedGame.transform.FindChild("Hardcore").gameObject.GetComponent<Text>();
                if (hardcore)
                {
                    hc.text = "Hardcore";
                    hc.color = Color.red;
                }
                else
                {
                    hc.text = "Softcore";
                    hc.color = Color.blue;
                }

                // Navigation
                Navigation nav = new Navigation();
                nav.mode = Navigation.Mode.Explicit;
                Button thisButton = savedGame.GetComponent<Button>();
                thisButton.navigation = nav;
                if (i > 0)
                {
                    Transform topObj = savedGame.transform.parent.transform.FindChild("Save " + (i - 1));
                    Button topButton = topObj.GetComponent<Button>();
                    nav.selectOnUp = topButton;
                    thisButton.navigation = nav;

                    Navigation topNav = new Navigation();
                    topNav.mode = Navigation.Mode.Explicit;
                    topNav.selectOnUp = topButton.navigation.selectOnUp;
                    topNav.selectOnDown = thisButton;
                    topButton.navigation = topNav;

                    if (i == files.Length - 1) //last saved game
                    {
                        Button backButton = GameObject.Find("BackButton").GetComponent<Button>();
                        nav.selectOnDown = backButton;
                        thisButton.navigation = nav;

                        Navigation backButtonNav = new Navigation();
                        backButtonNav.mode = Navigation.Mode.Explicit;
                        backButtonNav.selectOnUp = thisButton;
                        backButton.navigation = backButtonNav;
                    }
                }
                else
                {
                    GameObject.Find("MenuCanvas").GetComponent<KeyboardUIControl>().defaultSelectable = thisButton;
                }

                savedGame.GetComponent<ScollViewSelectableControl>().scroll =
                    GameObject.Find("SavesScrollView").GetComponent<ScrollRect>();
                savedGame.GetComponent<ScollViewSelectableControl>().smoothScroll =
                    GameObject.Find("SavesScrollView").GetComponent<SmoothScroll>();

                fin.Close();
            }
            catch (Exception ex)
            {
                Debug.Log(ex.ToString());
            }
        }


    }
}
