﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InGameMenu : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject optionPanel;
    public GameObject confirmationPanel;

    private Canvas ui;

    void Awake()
    {
        ui = GetComponent<Canvas>();
    }

    void Start()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (Time.timeScale == 0)
                Unpause();
            else if (Time.timeScale == 1)
                Pause();
        }
    }

    void OnEnable()
    {
        ToPauseMenu();
    }

    void Pause()
    {
        Time.timeScale = 0;
        ui.enabled = true;
        ToPauseMenu();
    }

    void Unpause()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
        Time.timeScale = 1;
    }

    void ToMainMenu()
    {
        Unpause();
        GameManager.instance.reset();
        GameManager.instance.Load("MainMenu", "main");
    }

    void ToPauseMenu()
    {
        pausePanel.SetActive(true);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
    }

    void ToOptionMenu()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(true);
        confirmationPanel.SetActive(false);
    }
}
