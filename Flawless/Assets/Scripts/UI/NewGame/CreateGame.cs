﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class CreateGame : MonoBehaviour
{
    public InputField gameName;
    public Dropdown difficulty;
    public Toggle hardcore;
    public Button createGameButton;

    private Text statusText;
    private List<string> saveNames = new List<string>();
    private string savePath = "./Saves/";
    private string saveType = ".txt";

    void Awake()
    {
        statusText = GetComponent<Text>();
    }

    void Start()
    {
        OnTextChange();

        // Get all save game names in Saves folder
        if (!Directory.Exists(savePath))
            Directory.CreateDirectory(savePath);
        DirectoryInfo saveDir = new DirectoryInfo(savePath);
        FileInfo[] files = saveDir.GetFiles("*" + saveType);
        foreach (FileInfo file in files)
            saveNames.Add(file.Name.Split('.')[0]);
    }

    public void OnTextChange()
    {
        if (gameName.text.Length == 0)
        {
            statusText.text = "Game name cannot be blank!";
            createGameButton.interactable = false;
        }
        else if (saveNames.IndexOf(gameName.text) > -1)
        {
            statusText.text = "Game name already existed";
            createGameButton.interactable = false;
        }
        else
        {
            statusText.text = "";
            createGameButton.interactable = true;
        }
    }


    public void Create()
    {
        GameSettings.instance.gameName = gameName.text;
        GameSettings.instance.difficulty = difficulty.captionText.text;
        GameSettings.instance.hardcore = hardcore.isOn;

        try
        {
            StreamWriter fout = new StreamWriter(savePath + GameSettings.instance.gameName + saveType);
            fout.WriteLine(GameSettings.instance.gameName);
            fout.WriteLine(GameSettings.instance.difficulty);
            fout.WriteLine(GameSettings.instance.hardcore ? 1 : 0);
            fout.Close();
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }
}
