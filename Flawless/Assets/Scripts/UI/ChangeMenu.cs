﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeMenu : MonoBehaviour
{
    public Button button;
    public string changeSceneName;
    public string changeSongName;

    void Start()
    {
        if (button != null)
            button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        if (changeSceneName == "Exit")
        {
            Application.Quit();
            return;
        }
        if(changeSceneName == "Continue")
        {
            GameManager.instance.LoadSave();
            return;
        }
        GameObject fader = GameObject.FindGameObjectWithTag("Fader");
        if (fader != null)
        {
            fader.BroadcastMessage("Enable");
            fader.BroadcastMessage("FadeOut");
            Invoke("LoadSceneImmidiately", .23f); //a bit faster than fade out time (0.25)
            return;
        }
        else LoadSceneImmidiately();
    }

    void LoadSceneImmidiately()
    {
        Application.LoadLevel(changeSceneName);
        SoundManager.instance.PlayMusic(changeSongName);
    }
}
