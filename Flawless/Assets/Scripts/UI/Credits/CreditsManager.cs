﻿using UnityEngine;
using System.Collections;

public class CreditsManager : MonoBehaviour
{
    private ChangeMenu changeMenu;

    void Awake()
    {
        changeMenu = GetComponent<ChangeMenu>();
    }

    void Start()
    {
        StartCoroutine(IWaitUntilEscape());
    }

    public IEnumerator IWaitUntilEscape()
    {
        while (true)
        {
            if (Input.anyKeyDown)
                break;
            yield return Time.fixedDeltaTime;
        }
        if (changeMenu != null)
            changeMenu.LoadScene();
    }
}
