﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour, HitAble
{
    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriterenderer;
    private Moveable moveable;
    private BoxCollider2D box;
    private HurtBox hurtBox;
    private HitBox hitBox;
    private LifeBar lifeBar;
    private GroundBox groundBox;

    private bool jumpCancel;
    private int flip;
    private int HP;
    private int maxHP;
    private bool attaking;
    private int Hitstun;
    private int invulnerable;
    private bool jumped;
    private bool fired;
    private bool fixGround;

    public int invincibilityFrames;
    public float moveSpeed;
    public float jumpSpeed;
    public int baseAttack;
    public float maxDownVelocity;
    public float minJumpSpeed;
    public float maxCancelSpeed;
    public float cancelSetSpeed;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriterenderer = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
        moveable = new Moveable(body, maxDownVelocity);
    }

    void Start ()
    {
        lifeBar = GameObject.Find("LifeBar").GetComponent<LifeBar>();
        if (!GameManager.instance.first)
            transform.position = new Vector2(GameManager.instance.newXpos, GameManager.instance.newYpos);
		HP = GameManager.instance.PlayerHp;
        maxHP = GameManager.instance.PlayerMaxHp;
        flip = GameManager.instance.PlayerFlip;
        if (flip == -1)
            moveable.flip(transform);

        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hitBox = transform.GetChild(1).GetComponent<HitBox>();
        groundBox = transform.GetChild(2).GetComponent<GroundBox>();
        hitBox.setEnable(false);
        hitBox.setDamage(baseAttack + GameManager.instance.GlobalAttackUp);
        hitBox.setParent(this);
        hurtBox.setParent(this);

    }
    void Update()
    {
        if (Time.timeScale == 0) return;
        if (Input.GetButtonDown("Jump"))
            jumped = true;
        if (Input.GetButtonDown("Fire1"))
            fired = true;
        fixGround = groundBox.isGround()&& body.velocity.y <= 0.05f;
        if(body.velocity.y > 0.05f)
            groundBox.off();
    }
    void FixedUpdate()
    {

        if (Hitstun != 0)
        {
            runHitstun();
        }
        else if (attaking)
        {
            runAttack();
            if (!fixGround)
                MoveHorizontal();
        }
        else
        {
            if (!Attack())
            {
                MoveHorizontal();
                JumpCheck();
                Jumped();
            }
            else
                JumpCheck();
        }
        runInvulnerable();
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Enemy") || hit.gameObject.tag.Equals("HitAll"))
        {
            if (Hitstun == 0 && invulnerable == 0)
            {
                animator.SetBool("Hit", true);
                runDamageCalcuation(hit);
                AttackRecovery();
                AttackOver();
                if (HP <= 0)
                {
                    spriterenderer.enabled = false;
                    GameObject.Find("GameOver").GetComponent<Image>().enabled = true;
                    lifeBar.clear();
                }
            }
        }
    }

    private void runDamageCalcuation(Collision2D hit)
    {
        HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
        HP -= gotHitBox.getDamage();
        if (HP < 0)
            HP = 0;
        lifeBar.updateHp(HP);
        int direction = 1;
        if (hit.gameObject.transform.position.x - transform.position.x < 0)
            direction = -1;
        moveable.moveY(gotHitBox.knockbackY);
        moveable.setForceX(gotHitBox.knockbackX * direction);
        Hitstun = gotHitBox.hitStun;
        animator.SetBool("Hitstun", true);
    }

    private bool Grounded()
    {
        float HalfWidth = (transform.lossyScale.x*box.size.x)/2.0f;
        return GroundRaycastCheck(0)|| GroundRaycastCheck(HalfWidth)|| GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x+offset, transform.position.y - HalfHeight), Vector2.down, .02f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            { 
                return true;
            }
        }
        return false;
    }

    private bool Attack()
    { 
        if (fired)
        {
            animator.SetBool("Attack", true);
            attaking = true;
            fired = false;
            return true;
        }
        return false;
    }
    private void MoveHorizontal()
    {
        float Horizontal = Input.GetAxisRaw("Horizontal");
        if (Horizontal != 0)
        {
            moveable.moveX(moveSpeed * Horizontal);
            animator.SetBool("Run", true);
            if (flip != Horizontal)
            {
                flip = (int)Horizontal;
                moveable.flip(transform);
            }
        }
        else
        {
            moveable.moveX(0);
            animator.SetBool("Run", false);
        }
    }
    private void Jumped()
    {
        if (jumped && fixGround)
        {
            moveable.moveY(jumpSpeed);
            jumpCancel = true;
            jumped = false;
            animator.SetBool("Jump", true);
            animator.SetBool("Peak", false);
            animator.SetBool("Fall", false);
        }
        if (Input.GetAxisRaw("Jump") == 0 && jumpCancel && body.velocity.y > maxCancelSpeed && body.velocity.y < minJumpSpeed)
        {
            moveable.moveY(cancelSetSpeed);
            jumpCancel = false;
        }
    }
    private void JumpCheck()
    {
        if (body.velocity.y < 1)
            animator.SetBool("Peak", true);
        if (body.velocity.y < -1 && !Grounded())
            animator.SetBool("Fall", true);
        if (fixGround)
        {
            animator.SetBool("Peak", false);
            animator.SetBool("Fall", false);
            animator.SetBool("Jump", false);
            animator.SetBool("Land", true);
        }
        else
            animator.SetBool("Land", false);

    }
    private void runAttack()
    {
        moveable.moveX(0);
    }
    private void runHitstun()
    {
        animator.SetBool("Hit", false);
        if (HP == 0)
            moveable.move(0,0);
        if (fixGround)
            moveable.moveX(0);
        else
            moveable.ForceX();
        if (Hitstun == 1)
        {
            if (HP <= 0)
            {
                GameManager.instance.reset();
                GameManager.instance.Load("MainMenu","main");
            }
            else
            {
                animator.SetBool("Hitstun", false);
                invulnerable = invincibilityFrames;
            }
        }
        Hitstun--;
    }
    private void runInvulnerable()
    {
        if (invulnerable != 0)
        {
            invulnerable--;
            if (invulnerable % 3 == 1)
                spriterenderer.enabled = false;
            else
                spriterenderer.enabled = true;
        }
    }

    public int getFlip() { return flip; }
    public int getHP() { return HP; }
    public void attackup()
    {
        hitBox.setDamage(baseAttack + GameManager.instance.GlobalAttackUp);
    }
    public void MaxHpup()
    {
        maxHP = GameManager.instance.PlayerMaxHp;
        lifeBar.AddMaxLife(maxHP);
    }
    public void Heal(int amount)
    {
        HP += amount;
        if (HP > maxHP)
            HP = maxHP;
        lifeBar.updateHp(HP);
    }
    public void AttackActive()
    {
        hitBox.setEnable(true);
    }
    public void AttackRecovery()
    {
        hitBox.setEnable(false);
    }
    public void AttackOver()
    {
        attaking = false;
        animator.SetBool("Attack", false);
    }
}
